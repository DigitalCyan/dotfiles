# dotfiles

This repo contains some if not all of my dotfiles used in customizing and optimizing my workflow.

## Supported DE/WM-s

* i3
* openbox
* xfce

## Dependencies

* picom
* thunar
* alacritty
* rofi
* flameshot
* unclutter

## Supported software and dependencies

### nvim

Dependencies: 
* vimplug

### tmux

Dependencies: 
* tpm


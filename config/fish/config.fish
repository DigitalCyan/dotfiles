# Override the greeting
function fish_greeting
	# Uncomment the next line if you're using Arch
	# neofetch
end

# Customize the prompt
function fish_prompt
	set_color 44864E;
	echo "$USER@$hostname: $PWD"
	echo "λ "
end

# Aliases
function vim
	nvim $argv
end

function l
	ls -l
end

function ll
	ls -la
end

# Path additions
set -a PATH "$HOME/.local/bin"
set -a PATH "$HOME/.yarn/bin"
set -a PATH "$HOME/.cargo/bin"
set -a PATH "$HOME/Scripts"
